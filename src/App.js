import React, { Component } from "react";
import { Button, Container, Row, Col, Jumbotron, Form } from 'react-bootstrap';
import  './App.css'
// exports
export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {money:'', notesValue:[2000, 500, 200, 100, 50, 20, 10, 5, 2, 1], validation:0};
  }

  // Set the input value inside the state
  __setFieldValue(event) {
    let value = event.target.value;
    if(isNaN(value)){
         this.setState({validation:1});       
    }else{
    this.setState({money:event.target.value, result:[], count:0, validation:0})
    return true;
    }
  }

  // Calculate the value
  __getMoney(event) {
      let money = this.state.money;
      let resultArray = [];
      let resultCount = 0;
      (this.state.notesValue).forEach((note, index)=>{
        if(money >= note){
          let count = parseInt(money/note);
          resultArray.push(`${count} notes of Rs ${note},`)
          money = money - (count * note);
          resultCount++;
        }
      })
      this.setState({result:resultArray, count:resultCount})
  }
  render() {
    return (
      <div className="App">
      <Container>
      <Jumbotron>
        <Row>
          <Form>
            <span><b>Welcome to ATM</b></span>
              <Form.Group controlId="formBasicEmail">
              <Form.Label>Enter the Amout</Form.Label>
              <Form.Control type="text" placeholder="Enter the amount" name="money" value={this.state.value} onChange={(e) => { this.__setFieldValue(e) }}/>
              <Form.Text className="text-muted">
                {this.state.validation === 1 ? 'Please enter only number value':null}
              </Form.Text>
              </Form.Group>
              <Col><Button variant="primary" onClick={(e) => { this.__getMoney(e) }}>Get Money</Button></Col>
          </Form>
          <Col>
          <p><b>You will get following amount</b></p>
             {this.state.result}
          <p><b>Total notes dispensed: {(this.state.count)}</b></p>
          </Col>
        </Row>
        </Jumbotron>
        </Container>
    </div>)}}

export default App;
